<?php
/*
	author : wahyu
*/
Class suratjalan extends CI_Model {

	function selectMax() {
		$this->db->select('MAX(idsuratjalan)');
		$this->db->from('suratjalan');

		$query = $this -> db -> get();
		return $query->result();
	}
	
	function add($data) {
		$this->db->insert('suratjalan', $data);
	}
	
	function update($id, $data) {
		$this->db->where('idsuratjalan', $id);
		$this->db->update('suratjalan', $data);
	}
}
?>