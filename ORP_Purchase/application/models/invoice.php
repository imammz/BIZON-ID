<?php
/*
	author : wahyu
*/
Class invoice extends CI_Model {

	function selectMax() {
		$this->db->select('MAX(idinvoice)');
		$this->db->from('invoice');

		$query = $this -> db -> get();
		return $query->result();
	}

	function findByQuotationSalesId($id) {
		$this->db->select('*');
		$this->db->from('invoice');
		$this->db->where('idquotationsales = ' . "'" . $id . "'");

		$query = $this -> db -> get();
		return $query->result();
	}
	
	function add($data) {
		$this->db->insert('invoice', $data);
	}
	
	function update($id, $data) {
		$this->db->where('idinvoice', $id);
		$this->db->update('invoice', $data);
	}
}
?>