
<?php echo Modules::run('front_templates/front_templates/header'); ?>
<?php echo Modules::run('front_templates/front_templates/menu'); ?>

<script type="text/javascript" src="<?php echo base_url() ?>front_assets/library/gb/greybox.js"></script>
<link type="text/css" href="<?php echo base_url() ?>front_assets/library/gb/greybox.css" rel="stylesheet" />
	  

<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Transaksi</h1>
      <ol class="breadcrumb">
	   <li><a href="index.html">Transaksi Penjualan</a></li>
        <li class="active">Invoice</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <!-- <a data-toggle="modal" data-target="#myModalAdd" class="btn btn-option1 btn-xl"><i class="fa fa-plus"></i>Add New</a> -->
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->



<!-- START CONTAINER -->
<div class="container-padding">
  <!-- Start Row -->
  <div class="row">
        <!-- Start Panel -->
        <div class="col-lg-17">
          <div class="panel panel-default">
            <div class="panel-title">
             Data Invoice
            </div>
            <div class="panel-body table-resdonsive">
                <table id="example1" class="table display">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor DO</th>
                            <th>Nomor PO</th>
                            <th>Tanggal</th>
                            <th>Pelanggan</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $ids=1;
                            foreach ($invoicepenjualan as $row) {
                        ?>
                                <tr>
                                    <td><?php echo $ids ?></td>
                                    <td><?php echo $row['nomordo'] ?></td>
                                    <td><?php echo $row['nomor'] ?></td>
                                    <td><?php echo Tanggal::formatDate($row['tanggal']) ?></td>
                                    <td><?php echo $row->pelanggan['namapelanggan'] ?></td>
                                    <td><?php echo $row->refstatus['status'] ?></td>
                                    <td>
                                        <!-- <a href="<?php echo base_url() ?>index.php/pemesanan/do_pemesanan/rollback/<?php echo $row['idquotationsales'] ?>" onclick="return confirm('Anda Yakin Ingin Rollback Data DO ini?')" class="btn btn-danger" data-toggle="tooltip" data-placement="top" data-original-title="Klik Untuk Rollback DO"><i class="fa fa-rotate-left"></i>Rollback</a> -->
                                        <a onclick="edit(<?php echo $row['idquotationsales'] ?>)" data-toggle="tooltip" data-target="#myModalUpdate" class="btn btn-option5" data-placement="top" data-original-title="Klik Untuk Melihat Detil"><i class="fa fa-edit"></i>Detail</a>
                                        <?php if ($row['flag_invoice'] == 0) { ?>
                                          <a href="#" onclick="addHiddenValue(<?php echo $row['idquotationsales'] ?>, <?php echo $row['idpelanggan'] ?>)" data-toggle="modal" data-target="#modalAddInvoice" class="btn btn-success" data-placement="top" data-original-title="Buat Invoice"><i class="fa fa-plus"></i>Buat Invoice</a>
                                        <?php } else { ?>
                                          <a href="#" onclick="cetakInvoice(<?php echo $row['idquotationsales'] ?>)" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Klik Untuk Cetak Invoice"><i class="fa fa-print"></i>Cetak Invoice</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                        <?php
                                $ids++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- End Panel -->
      </div>
  <!-- End Row -->
</div>
<!-- END CONTAINER -->


    <!-- Modal Tambah Invoice -->
    <div class="modal fade" id="modalAddInvoice" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content " >
                <form action="<?php echo base_url() ?>index.php/penjualan/invoice_penjualan/buatinvoice"  enctype="multipart/form-data" class="form-horizontal" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tambah Invoice</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="panel-body">
                                <div role="tabpanel">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="PO1">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" id="idquotationsales" name="idquotationsales">
                                                <input type="hidden" class="form-control" id="idpelanggan" name="idpelanggan">
                                                <label for="nomordo" class="col-sm-2 control-label form-label">Nomor Invoice</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="nomor_invoice" name="nomor_invoice">
                                                </div>
                                                <label for="tanggal" class="col-sm-2 control-label form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input type="text" id="date-picker" class="form-control date-picker" name="tanggal_invoice" value="<?php echo date('d-m-Y') ?>"/> 
                                                            </div>
                                                            <br/> <sup>)*Tgl-Bln-Thn</sup>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>              
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>   
            </div>
        </div>
    </div>
    <!-- Modal Tambah Invoice -->



<!-- Modal Add Detail Data -->
            <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail</h4>
                  </div>
                  <div class="modal-body">
            <div class="panel-body">
			        <div class="panel-body">
        
                <div role="tabpanel">

                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs tabcolor6-bg" role="tablist">
                    <li role="presentation" class="active"><a href="#dataDO" aria-controls="dataDO" role="tab" data-toggle="tab">:::Data Utama:::</a></li>
                    <li role="presentation"><a href="#databarang" aria-controls="databarang" role="tab" data-toggle="tab">:::Data Barang Detail:::</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="dataDO">
           <form class="form-horizontal">
			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Nomor DO</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
			
			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Tanggal DO</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
		   
		   
		   <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Nomor Reff</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
		   
		   		   <div class="form-group">
                    <label for="input4" class="col-sm-2 control-label form-label">Supplier</label>
                  <div class="col-sm-10">
                    <select class="selectpicker" data-style="btn-primary"  placeholder="Disable Input" disabled>
                        <option>ID 001</option>
                        <option>ID 002</option>
                        <option>ID 003</option>
                      </select>                  
                  </div>
            </div>
       </form> 
</div>
        
		<div role="tabpanel" class="tab-pane" id="databarang">
		 <form class="form-horizontal">
            <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>

			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Jumlah Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
			
			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Gudang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
			
			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Ukuran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
			
			<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Keterangan</label>
                  <div class="col-sm-10">
                    <input type="textarea" class="form-control" id="input002"  placeholder="Disable Input" disabled>
                  </div>
            </div>
			
                    </div>
                  </div>

                </div>              

            </div>
 </form>
            </div>
                  </div>
                </div>
              </div>
            </div>
<!-- Modal Add Detail Data -->


<!-- Print Data -->
            <div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirmation Print</h4>
                  </div>
                  <div class="modal-body">
                    Are You Sure To Print This Item
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-info">Print</button>
                  </div>
                </div>
              </div>
            </div>
<!-- Print Data -->

<!-- Modal Add Update -->
    <div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-hidden="true">
        <input type="hidden" id="hididquotationsales" />
        <div class="modal-dialog modal-lg">
            <div class="modal-content " >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detil Data</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="panel-body">
                            <form id="formUpdate" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/quotation/quotation/update">

                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tabcolor8-bg" role="tablist">
                                        <li role="presentation" class="active"><a href="#PO1Update" aria-controls="PO1Update" role="tab" data-toggle="tab">:::Penawaran:::</a></li>
                                        <li role="presentation"><a href="#PO2Update" aria-controls="PO2Update" role="tab" data-toggle="tab">:::Detil Barang:::</a></li>
                                    </ul>
                                    <!-- Nav tabs -->

                                    <!-- Tab panes -->
                                    <div class="tab-content">

                                        <div role="tabpanel" class="tab-pane active" id="PO1Update">
                                            <div class="form-group">
                                                <label for="nomor" class="col-sm-2 control-label form-label">Nomor Quotation</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="nomordoupdate" name="nomor" disabled="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="tanggal" class="col-sm-2 control-label form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input type="text" id="date-picker-update" class="form-control date-picker" name="tanggal" value="<?php echo date('d-m-Y') ?>" disabled="true"/> 
                                                            </div>
                                                            <br/> <sup>)*Tgl-Bln-Thn</sup>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="idpelangganupdate" class="col-sm-2 control-label form-label">Pelanggan</label>
                                                <div class="col-sm-10">
                                                    <div class="col-xs-12" style="text-align: left">
                                                    <select name="idpelanggan" id="idpelangganupdate" class="selectpicker" data-live-search="true" disabled="true">
                                                        <?php foreach ($this->orm->pelanggan() as $row) { ?>
                                                            <option value="<?php echo $row['idpelanggan'] ?>"> <?php echo $row['namapelanggan'] ?> </option>
                                                        <?php } ?>    
                                                    </select>
                                                    </div>  
<!--                                                    <div class="col-xs-6" style="text-align: left"><a href="#" onclick="tambahPelanggan()" class="btn btn-primary btn-xs" >Tambah Data Pelanggan</a></div>-->
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="idrefjenispembayaranupdate" class="col-sm-2 control-label form-label">Jenis Pembayaran</label>
                                                <div class="col-sm-10">
                                                    <div class="col-xs-12" style="text-align: left">
                                                    <select name="idrefjenispembayaran" id="idrefjenispembayaranupdate" class="selectpicker" data-live-search="true" disabled="true">
                                                        <?php foreach ($this->orm->refjenispembayaran() as $row) { ?>
                                                            <option value="<?php echo $row['idrefjenispembayaran'] ?>"> <?php echo $row['jenispembayaran'] ?> </option>
                                                        <?php } ?>    
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="dibuat_oleh_update" class="col-sm-2 control-label form-label">Dibuat Oleh</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="dibuat_oleh_update" value="<?php echo $_SESSION['user']['namalengkap'] ?>" name="dibuat_oleh" disabled="true">
                                                </div>
                                            </div>

                                        </div>

                                        <div role="tabpanel" class="tab-pane" id="PO2Update">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <hr/>
                                                        <table id="pemesanan_barang_update" width="100%">
                                                            <tr bgcolor="white">
                                                                <td width="25px"><b>|#|</b></td>
                                                                <td width="35px"><b>|No|</b></td>
                                                                <td width="330px"><b>Nama Barang</b></td>
                                                                <td width="150px"><b>Jumlah Barang</b></td>
                                                                <td width="150px"><b>Harga Jual Satuan</b></td>
                                                                <td width="350px"><b>Keterangan</b></td>
                                                            </tr>
                                                        </table>
                                                        <br/>                                                 
                                                    </td>
                                                </tr> 
                                            </table>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal" onclick="removeTrTag();">Tutup</button>
                                            <!-- <button type="submit" class="btn btn-success">Simpan</button> -->
                                        </div>

                                    </div>

                                </div>   
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add Update -->
</div>

<?php echo Modules::run('front_templates/front_templates/footer'); ?>
<?php echo Modules::run('front_templates/front_templates/js_master'); ?>

<script>
    $(document).ready(function() {
        $('#example1').DataTable();
        $('#date-picker').datepicker({
          autoclose: true
        });
    });

    function edit(idquotation) {
        var url = "<?php echo base_url() ?>index.php/pemesanan/do_pemesanan/ajaxEdit/"+idquotation;
        $.ajax({
            type: "json",
            url: url,
        }).success(function(data) {
            $("#formUpdate").attr("action", "<?php echo base_url() ?>index.php/quotation/quotation/update/"+idquotation);
            $("#nomordoupdate").val(data.content.sales[0].nomor);
            $("#date-picker-update").val(data.content.sales[0].tanggal);
            $("#idrefjenispembayaranupdate [value='"+data.content.sales[0].idrefjenispembayaran+"']").prop("selected", true);
            $("#idpelangganupdate [value='"+data.content.sales[0].idpelanggan+"']").prop("selected", true);
            $("#dibuat_oleh_update").val(data.content.sales[0].dibuat_oleh);
                                                                    
            var obj = data.content.salesdetail;
            $.each(obj, function(i, val) {                                                                       
                $("#pemesanan_barang_update").append("<tr class='willremove'>"+
                    "<td width='25px'><input type='checkbox' size='1' name='chk' disabled='true' /></td>"+
                    "<td width='40px'><input class='form-control' type='text' size='3' id='urutan_jenis_barang_1' name='urutan_jenis_barang[]'' value='"+(i+1)+"' disabled='true' /></td>"+
                    "<td width='350px'>"+
                        "<select id='idrefbarang' name='idrefbarang[]' class='form-control' disabled='true'>"+
                            "<?php"+
                            "foreach ($this->orm->refbarang->order('idrefjenisbarang ASC') as $row) {"+
                              "echo '<option value='"+val.idrefbarang+"'>"+val.namabarangtext+"</option>';"+
                            "}"+
                            "?>"+
                        "</select>"+
                    "</td>"+
                    "<td width='150px'><input class='form-control' type='number' id='jumlahbarang' name='jumlahbarang[]' value='"+val.jumlahbarang+"' disabled='true' /></td>"+
                    "<td width='150px'><input class='form-control' type='number' id='hargasatuan' name='hargasatuan[]' value='"+val.hargasatuan+"' disabled='true' /></td>"+
                    "<td width='150px'><input class='form-control' type='text' id='keterangan_1' name='keterangan[]' value='"+val.keterangan+"' disabled='true' /></td>"+
                    "<input class='form-control' type='hidden' id='idquotationsalesdetail"+(i+1)+"' name='idquotationsalesdetail[]' value='"+val.idquotationsalesdetail+"' />"+
                    "</tr>");
            });
            $("#myModalUpdate").modal("show");
        });
    }

    function cetakInvoice(idquotationsales) {
        GB_show("Cetak Invoice", '<?php echo base_url() ?>index.php/penjualan/invoice_penjualan/cetak/' + idquotationsales, 500, 1080);
        $('html, body').animate({scrollTop: 0}, 'slow');
    }

    function removeTrTag() {
        $("tr.willremove").remove();
    }

    function addHiddenValue(values1, values2) {
      $("#idquotationsales").val(values1);
      $("#idpelanggan").val(values2);
    }
</script>

<link href="<?php echo base_url() ?>front_assets/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>front_assets/js/bootstrap-datepicker.js"></script>
