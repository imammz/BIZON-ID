/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function addRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var colCount = table.rows[1].cells.length;

    for(var i=0; i<colCount; i++) {

        var newcell = row.insertCell(i);

        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
        //alert(newcell.childNodes[0].type);
        switch(newcell.childNodes[0].type) {
            case "text":
                newcell.childNodes[0].value = "";
                break;
            case "number":
                newcell.childNodes[0].value = "";
                break;    
            case "checkbox":
                newcell.childNodes[0].checked = false;
                break;
            case "select-one":
                newcell.childNodes[0].selectedIndex = 0;
                break;
        }
        switch(newcell.childNodes[0].name) {
            
            
            case "urutan_jenis_barang[]":
                newcell.childNodes[0].value= (rowCount);
                newcell.childNodes[0].setAttribute('id','urutan_jenis_barang_'+(rowCount-1));
                //alert(rowCount);
                break;
            
            case "idRefBarang[]":
                newcell.childNodes[0].setAttribute('title','idRefBarang_'+rowCount);
                newcell.childNodes[0].setAttribute('id','idRefBarang_'+rowCount);
                break;
            case "expired_date[]":
                newcell.childNodes[0].setAttribute('title','expired_date_'+rowCount);
                newcell.childNodes[0].setAttribute('id','expired_date_'+rowCount);
                break;
            
            case "ukuranDetail[]":
                newcell.childNodes[0].setAttribute('title','ukuranDetail_'+rowCount);
                newcell.childNodes[0].setAttribute('id','ukuranDetail_'+rowCount);
                break;    
            case "jenisBarangDetail[]":
                newcell.childNodes[0].setAttribute('title','jenisBarangDetail_'+rowCount);
                newcell.childNodes[0].setAttribute('id','jenisBarangDetail_'+rowCount);
                break;    
                
            case "jumlahBarang[]":
                newcell.childNodes[0].setAttribute('title','jumlahBarang_'+rowCount);
                newcell.childNodes[0].setAttribute('id','jumlahBarang_'+rowCount);
                newcell.childNodes[0].value = 0;
                break;
            
            case "stock[]":
                newcell.childNodes[0].setAttribute('title','stock_'+rowCount);
                newcell.childNodes[0].setAttribute('id','stock_'+rowCount);
                break;

            case "keterangan[]":
                newcell.childNodes[0].setAttribute('title','keterangan_'+rowCount);
                newcell.childNodes[0].setAttribute('id','keterangan_'+rowCount);
                break;



        }
    }
}

function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        for(var i=0; i<rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if(null != chkbox && true == chkbox.checked) {
                if(rowCount <= 2) {
                    alert("Tidak bisa menghapus semua baris\nCukup kosongkan field agar nilai\ntidak tersimpan!!!");
                    break;
                }
                table.deleteRow(i);
                rowCount--;
                i--;
            }

        }
    }catch(e) {
        alert(e);
    }
}

